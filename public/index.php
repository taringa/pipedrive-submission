<?php
require __DIR__ . '/../vendor/autoload.php';
include __DIR__ . '/../libs/PipedriveAPIClient.php';
include __DIR__ . '/../libs/DataValidator.php';
include __DIR__ . '/../libs/JSONResponse.php';

/*
	//Fake Input data, uncomment this lines for testing from CLI
	$_POST['name'] = 'Apellido y Nombres';
	$_POST['company'] = 'Tadinga';
	$_POST['country'] = 'American Samoa';
	$_POST['email'] = 'apellido@tadinga.net';
	$_POST['phone'] = '11 4855 9371';
	$_POST['budget'] = 5000;
	$_POST['company_type'] = 'Anunciante';

	$_SERVER['REQUEST_METHOD'] = 'POST';
*/

//Make sure we only process POST requests.
if ($_SERVER['REQUEST_METHOD'] != 'POST'){
	json_response(400, array("message"=>"This endpoint only accepts POST payloads"));
	exit();
}

//Read data validation rules and initialize validator
$dv_config = include  __DIR__ . '/../config/DataValidatorConfiguration.php';
$dv = new DataValidator($dv_config);
$validated_data = $dv->run($_POST);


if($validated_data === false) {

	//If there´s any validation issue, send an error message with details
	json_response(400, array("message"=>$dv->get_error_messages()));
	
} else {
	
	//Otherwise, initialize a Submit2Pipedrive connector and send the submission
	$validated_data['note_text'] = "Monto de Inversion Mensual: $".$_POST['budget']."\nTipo de Empresa: ".$_POST['company_type']."\nPais: ".$_POST['country'];
	
	//Read pipedrive configuration and initialize API Client
	$api_config = include  __DIR__ . '/../config/PipedriveAPIConfiguration.php';
	$pipedrive = new PipedriveAPIClient($api_config['pd_key'],$api_config['pd_deal_owner'], $api_config['pd_note_prefix']);
	
	try {
		//Attempt submission to pipedrive
		$pipedrive->new_submission($validated_data['name'],$validated_data['company'],$validated_data['email'],$validated_data['phone'],$validated_data['note_text']);
		
		//Everything went ok
		json_response(200, array('message'=>'Submission succesful'));
		
	} catch (Benhawker\Pipedrive\Exceptions\PipedriveApiError $e) {
		//There where authentication problems with Pipedrive.
		json_response(500, array('message'=>$e->getMessage()));
		
	} catch (Benhawker\Pipedrive\Exceptions\PipedriveHttpError $e) {
		//There where connection problems with Pipedrive's API.
		json_response(500, array('message'=>$e->getMessage()));
		
	} catch (Exception $e) {
		//Unknown exceptions
		json_response(500, array('message'=>$e->getMessage()));
		
	}
	
}
