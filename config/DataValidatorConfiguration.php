<?php
return array(
	'name'			=> 'required|max_len,150',
	'company'		=> 'required|max_len,150',
	'country'		=> 'required|max_len,150',
	'email'			=> 'required|valid_email',
	'phone'			=> 'required',
	'budget'		=> 'required',
	'company_type'	=> 'required'
);