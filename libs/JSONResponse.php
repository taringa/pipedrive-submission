<?php
function json_response($code, $data){
	http_response_code($code);
	$result = false;
	if ($code === 200) $result = true;
	echo json_encode(array("result"=>$result,$data));	
}