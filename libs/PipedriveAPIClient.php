<?php
use Benhawker\Pipedrive\Pipedrive;

class PipedriveAPIClient {
	
	var $pd;
	var $deal_owner_userid;
	var $data_prefix;
	
	function __construct($pd_key, $deal_owner_userid,$data_prefix){
		$this->pd = new Pipedrive($pd_key);
		$this->deal_owner_userid = $deal_owner_userid;
		$this->data_prefix = $data_prefix;
		
	}
	
	function new_submission($contact_name, $contact_company, $contact_email, $contact_phone, $note_text){
				
		//add organization
		$organization['name'] = $contact_company;
		$organization = $this->pd->organizations()->add($organization); 
		
		//add customer
		$person['name']		= $contact_name;
		$person['email']	= $contact_email;
		$person['phone']	= $contact_phone;
		$person['org_id']	= $organization['data']['id'];
		
		$person = $this->pd->persons()->add($person);

		//add deal to user
		$deal['title']		= "[$this->data_prefix] " . $contact_name;
		$deal['stage_id']	= 1;
		$deal['user_id']	= $this->deal_owner_userid;
		$deal['person_id']	= $person['data']['id'];
		$deal['org_id']		= $organization['data']['id'];
		
		$deal = $this->pd->deals()->add($deal);
		
		//add note to customer
		$note['content']	= "[$this->data_prefix] " .$note_text;
		$note['person_id']	= $person['data']['id'];
		$note['org_id']		= $organization['data']['id'];
		$note['deal_id']	= $deal['data']['id'];
		
		$this->pd->notes()->add($note);

	}
}