<?php
class DataValidator {

	var $gump;
	
	function __construct($rules){
		$this->gump = new GUMP();
		$this->gump->validation_rules($rules);

	}
	
	function run($data){
		return $this->gump->run($data);
	}
	
	function get_error_messages(){
		
		$errors = array();
		foreach( $this->gump->get_errors_array() as $key => $value){
			$errors[] = array("field_name"=>$key, "error_message"=>$value);
		}
		return $errors;
	}
}