# PipeDrive Submission
This script parses and validates POST data from a web form or any internet application
and sends it to the PipeDrive CRM for follow-up from sales representatives.

It uses wixel/gump for data validation (see libs/DataValidator.php) and benhawker/pipedrive 
for Pipedrive´s API Client (see libs/PipedriveAPIClient.php). Composer is needed to install 
and include this libraries.

The script responds with a JSON message and an HTTP status code (see libs/JSONResponse.php).

##Installation Instructions
* Clone repo: git clone git@bitbucket.org:taringa/pipedrive-submission.git
* Install composer: https://getcomposer.org/download/
* Install dependencies: composer install
* Put your API Token and User ID in config/PipedriveAPIConfiguration.php
* Configure validation rules in config/DataValidatorConfiguration.php

### Don´t know your API Token?
* Log in to your Pipedrive account
* Click on your name in the top left corner, then Settings
* Click on Personal, and then API
* Copy the API token

### Need help with validation rules?
* Check out GUMP's docs at https://github.com/Wixel/GUMP#available-validators

##Usage Instructions
* Set up a webserver pointed to public/ (for example at http://localhost:3000/)
* Send a POST request to http://localhost:3000/ with the required payload
* Profit
